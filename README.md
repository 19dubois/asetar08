# PPE 3 BTS SIO2 - SLAM :  Projet "Asetar08" 2020

mdp= 6FZFanjI6dpePI7A
Projet ayant pour but la création d'un site internet et de ses différentes fonctionnalités, ainsi que d'une application.

Début de la réalisation du projet le 03/11/2020,
 
 Durée du projet estimé à environ 7 semaines.

© Copyright :

 - DUBOIS Marcellin
 - MANTZ Mathis
 - MEUNIER Jean
 ## Sommaire
 - **Informations**
 - **Analyse**
	 - Guide des bonnes pratiques
	 - Cas d’utilisation du site « Asetar 08 »
	 - Partie Juridique
	 - Les principales opérations comptables
	 - Logiciel
	 - Diagramme de classe UML
	 - Structure de la base de données
 
 ## Informations
 Le nom de domaine du site est www.asetar08.net.
 

> Les fonctionnalités demandées: 
> - Gestion des membres (après cotisation) 
> - Espace personnalisé pour les membres 
> - Gestion d’un espace public 
> - Ventes de « googdies », articles, … (via Paypal ou dans les locaux de l’association) 
> - Création et gestion d’équipes (tournois …) 
> - Gestion de la comptabilité pour les membres du bureau - gestion d’un ‘blog‘ étudiant - Envoi de SMS


> Le projet doit utiliser:
> - Service Web Apache 2 ( MPM Prefork 2.4) 
> - Service SGBD MariaDB 10.5 (MyISAM et INNODB) 
> - Service MongoDB NOSQL 4.4 
> - Service FTP (ProFTP 1.3.7) - Modules divers PHP 7.x

## Analyse

### Guide des bonnes pratiques:
-   Ne pas se ruer sur le code
-   Versionner le projet via GIT
-   Être méthodique
-   Commenter le code
-   Établir un cahier des charges fonctionnel
-   Faire un compte rendu régulier des tâches en cours ou accomplies
-   Dialoguer entre les membres du groupe
-   Ne pas se lancer dans plusieurs tâches en même temps
-   Etre au norme W3C (World Wide Web Consortium)
-   Respecter la RGPD
-   Ne pas improviser des choses, sans avertir le groupe
-   Utiliser la méthode SCRUM
-   Demander l’avis régulièrement au client
-   Regarder régulièrement les news informatiques
-   Tester le site avant de le donner au client
-   Aller à l’essentiel et ne pas chercher compliqué

### Cas d'utilisation du site « Asetar 08 »
|  **Cas d’utilisation**|**Fonctionnalitées pouvant être associé**  |
|--|--|
|Authentification (Membres)  | Connexion, Déconnexion, Récupération, Cookies,  Ajout de captcha|
|Création Compte (Non membres)|Activation, Envoie mail de confirmation, Accès aux conditions d'utilisations|
|Compte (Membres)|Modification, Gestion, Liste des produits achetés, Ajout image, Avis / Commentaires,  Favoris, Changer le numéro de téléphone, Suppression|
|Paiements (Administrateur(Gestion), Membres)|Paiement Paypal (associé à la boutique), Confirmation d’achat par mail, Gestion du système de paiement|
|Gestion des membres (Administrateur/Modérateur)|Affichage de tous les membres ayant cotisé, Date début de l’abonnement|
|Espace public(Tous, modifié par l'administrateur)| Page d’accueil, Événement d’actualité étudiante avec espace commentaire pour en discuter, Lien d’accès à divers blogs, Lien d’accès au(x) tournoi(s) du moment, Lien vers une boutique, Lien vers accès au profil de l’utilisateur, Accès EULA / conditions d’utilisation|
|Boutique (Tous sauf non membres)|Ajout au panier, Liste des produits|
|SMS (Pour les Membres)|Envoi sms de confirmation/rappel d’un événement|
|Tournois (Tous sauf non membres)|Création d’équipes, Tableau des scores, Arbre, Notification d’ajout d’un événement, Rappel de participation 24h avant d’un événement, Inscription, Désistement, Information, Règles, Date, Payant|
|Comptabilité (Administrateur)|Gestion de facture, Graph des revenues, Coûts prévisionnel d’un événement, Système de calcul automatique|
|Blog (Tous sauf non membres sauf droit de visibilité)|Public, Privé, Groupe, Publier, Ecrire, Lire, Créer, Supprimer, Droits|


### Partie Juridique

#### Le propriétaire


La loi explique clairement qu’un site web appartient à la personne qui l’a conçu. Donc si un client demande à une webagency de réaliser un site web, c’est cette dernière qui en est la propriétaire.
En effet, si le contrat stipule qu’il n’y a pas de transfert de propriété du prestataire vers le client, c’est l’agence web qui conserve l’entière propriété du site web. En revanche, si ce transfert n’est pas stipulé, la justice penche plutôt en faveur du client comme étant propriétaire du site.


#### La protection des données
Pour sécuriser au mieux son site Internet on peut :
- Choisir un hébergeur de qualité, se renseigner sur la fiabilité de celui-çi. Le bon hébergeur web met à votre disposition des fonctionnalités exceptionnelles comme :
	- Une plateforme intelligente CMS ;                                           
	- Une variété de versions de PHP, Python, Node.js, Ruby ;
	- Un espace disque illimité.
- Privilégier le HTTPS au HTTP. Si l’utilisation de notre site web amène les visiteurs à nous envoyer des données sensibles ou confidentielles, utiliser le HTTPS constitue la meilleure manière de protéger ces dernières.
- Assurer la mise à jour de ses logiciels, il faut toujours s’assurer de les installer afin de supprimer toute faille dans un logiciel.
- Prêter attention aux messages d’erreur. Il faut communiquer le strict minimum d’erreurs aux utilisateurs de notre site afin d’éviter que certains divulguent les secrets (clés API, mots de passe des bases de données…) de notre serveur en cas d’erreur.
- Tester le site avec les outils de sécurisation de sites web. Ces derniers testent tous les virus connus sur notre site afin de relever les différentes failles présentes. Ainsi, une fois ces dernières détectées, on peut prendre les mesures adéquates pour les supprimer.
- Éviter les habitudes compromettant la sécurité des sites web, c’est à dire éviter :
	- D’utiliser des services non sécurisés ;
	- D’utiliser les serveurs comme poste de travail en naviguant sur des sites web, en accédant à la messagerie électronique ;
	- De vous servir de comptes utilisateurs génériques. 


#### La vente en ligne


Chaque site internet e-commerce édité par un professionnel doit obligatoirement indiquer les mentions obligatoires suivantes :


- nom et prénom pour les personnes physiques, dénomination sociale pour les personnes morales ;
- adresse de l’entreprise,
- adresse e-mail de l’entreprise et coordonnées téléphoniques permettant d’entrer effectivement en contact avec elle,

Ensuite, suivant la situation, il convient d’indiquer :
- le capital social,
- le siège social,
- le numéro d’immatriculation au registre du commerce ou au registre des métiers,
- le numéro d’identification à la TVA,
- si l’activité est soumise à une autorisation, le nom et l’adresse de l’autorité qui lui a délivré cette autorisation,
- si la profession est réglementée, la référence aux règles professionnelles applicables, le titre professionnel, l’État dans lequel il a été octroyé, ainsi que le nom de l’organisme professionnel auprès duquel l’entreprise est inscrite.


Les auto-entrepreneurs dispensés d’immatriculation doivent indiquer sur le site la mention suivante : « dispensé d’immatriculation en application de l’article L. 123-1-1 du code de commerce ».


L’accès à ces informations doit être libre. En pratique, ces informations sont souvent regroupées au sein d’une page du site intitulée « mentions légales ».


L’entreprise doit également indiquer sur son site e-commerce les moyens de paiement acceptés et les éventuelles restrictions de livraison, au plus tard au début du processus de commande.

##### L’information des utilisateurs pour les contrats conclus à distance


Lorsque les produits ou services proposés sur le site ne s’adressent pas exclusivement aux professionnels, l’utilisateur doit obtenir communication, de manière lisible et compréhensible, des informations suivantes avant la confirmation de sa commande :


- l’absence de droit de rétractation ou le droit de rétractation (conditions, délai et mise en œuvre),
- le cas échéant, le fait que le consommateur supporte les frais de renvoi en cas de rétractation,
- l’information sur l’obligation du consommateur de payer des frais lorsque celui-ci exerce son droit de rétractation d’un contrat de prestation de services dont il a demandé expressément l’exécution avant la fin du délai de rétractation,
- l’adresse, le numéro de téléphone, le numéro de fax et l’adresse mail de l’entreprise,
- le coût éventuel de la communication à distance pour conclure le contrat,
- le cas échéant, la durée minimale des obligations contractuelles du vendeur,
- le cas échéant, les cautions et autres garanties à payer,
- le cas échéant, la possibilité de recourir à une procédure extrajudiciaire pour régler un litige avec - l’entreprise,
et, le cas échéant, l’existence de codes de conduite applicables.


La plupart de ces informations figurent en général au sein d’une page du site intitulée « conditions générales de vente (CGV) / conditions générales d’utilisation (CGU) ». En pratique, l’utilisateur doit avoir l’obligation de les lire et de les accepter pour pouvoir confirmer sa commande.


L’acheteur en ligne dispose d’un délai de rétractation de 14 jours à compter du jour qui suit l’acceptation de l’offre de services ou la livraison du bien. Il existe toutefois quelques exceptions à ce droit de dérogation, notamment pour la presse, les biens confectionnés sur-mesure et les biens périssables.


##### La réglementation des commandes passées par internet
La commande de l’utilisateur doit passer par 3 étapes :
- l’accès au détail de la commande et à son prix total,
- la possibilité de corriger les erreurs sur la commande si nécessaire,
- la confirmation de la commande.


##### L’entreprise doit informer l’utilisateur durant le processus de commande :
- des langues proposées pour conclure le contrat,
- de la durée du contrat si nécessaire,
- de la date à laquelle il s’engage à livrer le bien ou exécuter la prestation, sauf s’il s’agit de services financiers,
- de la durée minimale des obligations du client au titre du contrat,
- des étapes à suivre pour conclure le contrat,
- des moyens mis à disposition de l’utilisateur pour identifier et corriger ses éventuelles erreurs,
- des conditions d’accès aux contrats conclus,
- du système d’archivage des contrats utilisés par l’entreprise (pour toute commande supérieure à 120 euros, - l’entreprise est tenue de conserver le contrat pendant 10 ans minimum),
- de l’accès aux éventuelles règles professionnelles que l’entreprise doit respecter.


L’offre doit également comporter une durée de validité et il faut la retirer lorsque l’échéance est atteinte. A défaut, l’offre est réputée toujours valable.
Les prix indiqués sur le site doivent être clairs et il faut indiquer si les taxes et frais de livraison sont inclus dans le prix ou non.
Suite à la conclusion du contrat, la livraison doit intervenir au plus tard 30 jours après la commande en ligne.


##### La sécurisation des paiements à distance
L’entreprise exploitant un site e-commerce doit également respecter plusieurs règles en matière de paiement à distance.
Tout d’abord, en cas d’utilisation frauduleuse d’une carte bancaire, c’est à l’entreprise de supporter le risque. La banque peut débiter d’office le compte de l’entreprise pour tout achat contesté par écrit par le titulaire de la carte bancaire. Ce droit figure dans la convention de vente à distance liant l’entreprise à la banque.
Ensuite, l’entreprise doit également assurer la sécurité et la confidentialité des données bancaires de ses clients. Certaines informations sur le client ne pourront être conservées qu’avec le consentement de ce dernier (numéro de carte, expiration…).


##### La déclaration à la CNIL
Les sites commerciaux qui collectent des informations nominatives (nom ou adresse mail par exemple) et constituent des fichiers de clients et de prospects, doivent effectuer, suivant les cas, une déclaration simplifiée ou une déclaration normale auprès de la Cnil.
L’entreprise doit respecter les obligations suivantes au sujet des informations collectées sur les clients :
- elle doit obtenir leur accord au préalable,
- elle doit informer les clients de leur droit d’accès, de modification et de suppression de ces données,
- elle doit veiller à la sécurité et à la confidentialité des données,
et elle doit fournir la durée de stockage de ces données.



### Les principales opérations comptables
- Opérations liées à la boutique
- Opérations liées à l’inscription au site (devenir membre)
- Opérations liées à l’organisation des tournois (équipements, lots/cashprize, consommation…)

### Logiciel
#### Logiciel de gestion d'un site web
Liste non exhaustive des principaux logiciels de gestion d’un site Web :
-   Spip (multilinguisme)
-   Phpboost (très complet et simple d’utilisation, opensource)
-   Joomla
-   Ibexa (plus axée sur le marketing, e-commerce, libre service
-   CMS made simple (gestion rapide et facilité, opensource)
-   Typo3 (une grande variété de fonctionnalités,opensource)
-   OpenCms (Facile d’utilisation, opensource)
-   Wordpress (facile d’utilisation, aisément personnalisable, opensource)
-   Drupal (permet de publier et gérer facilement du contenu) 


Le plus important est de choisir son logiciel en fonction des besoins du projet, il faut par exemple :

- Définir la complexité du projet et choisir son logiciel en fonction.
- Prioriser tel ou tel aspect du site (choisir Typo3 comme logiciel pour un maximum de fonctionnalités mais choisir Spip pour simplifier la mise en forme par exemple).
- Evaluer la notoriété du logiciel, c’est à dire choisir celui qui rassemble la plus grande communauté d’utilisateurs pour avoir accès à de multiples tutoriels et faire évoluer son gestionnaire.


### L'architecture réseau du site Asetar08
![Architecture_réseau](./UML/Architecture_Reseau.png)


#### Les principaux framework Web
Un framework est une librairie déjà faite et qui possède différents outils permettant par exemple dans le cadre de création d’un site web, de fournir des outils CSS, Js ou encore PHP déjà défini dans le but de faciliter la création d’un site web.


Les principaux framework Web existants sont:
- Express
- Django
- Ruby on Rails
- Laravel
- Spring
- Angular
- React
- Vue
- Ember

#### Les notions
Définitions des notions de “FrontOffice” et “BackOffice” :
-   Le FrontOffice  constitue la partie d’un système informatique accessible aux utilisateurs finaux ou aux clients. Autrement dit cela concerne tout ce qui constitue l’aspect graphique.
- Le BackOffice  par opposition est la partie qui n’est pas accessible aux utilisateurs finaux ou aux clients. Autrement dit cela concerne tout ce qui constitue l’aspect technique.

#### Cocomo II
COCOMO II (acronyme de l’anglais COnstructive COst MOdel) est un modèle permettant de définir une estimation de l’effort à fournir dans le développement d’un logiciel et la durée que ce dernier prendra en fonction des ressources allouées. Le résultat de ce modèle n’est qu’une estimation il n’est pas parfaitement exact. 


Il est divisé en trois modèle : 
- Le modèle de base, qui effectue un simple calcul de l’effort et de la durée en fonction du nombre d’instructions que l’application doit contenir et la complexité de cette dernière.
- Le modèle intermédiaire prend en compte les paramètres précédents en y ajoutant les facteurs de coût (compétence de l’équipe, complexité de l’environnement technique ect..)
- Le modèle détaillé, prend en compte les paramètres du modèle intermédiaire en affinant les facteurs de coût en fonction de chaque phase du cycle de développement (n’est utile que pour d’imposants projets).


La complexité des applications à développer peuvent être de trois types : S (organique) correspondant aux applications simples et déterministes, P (semi-détaché), elles restent déterministes mais sont plus complexes, E (embarqué), elles ne sont pas déterministes et sont très complexes en terme de contraintes ou des données saisies (comme une certaine interface graphique si l’on ne peut pas envisager toutes les possibilités de saisies qu’un utilisateur pourrait effectuer).


COCOMO propose également un dispositif de distribution par phase permettant de déterminer le temps de développement et l’effort nécessaire pour chaque phase du cycle de développement, celui-ci étant répartis en 4 grandes phases :
- Expression des besoins et planification
- Conception générale 
- Programmation (avec conception détaillée, programmation et tests unitaires)
- Tests et intégration   


![UML1](./UML/Packages.png)


Nous avons fait le calcul en comptant dans modele nous avons 5 classes de 200 lignes, dans modele et controller nous avons 12 classes de 100 lignes, 200 lignes en CSS et JS. Donc cela nous donne 2400 lignes.


![cocomoII paramètres](./cocomoII/Screenshot_20201118_145617.png)


![cocomoII result](./cocomoII/Screenshot_20201118_145952.png)


### Diagramme de classe UML


![Diagramme classe](./UML/DiagrammeClasse.png)


### Structure de la base de données


![Base de données](./UML/UMLbdd.png)


![mascotte](https://s2.qwant.com/thumbr/0x380/5/d/49208fe213b938d33b4bb07813d9ca097c1946eb18de73a85d9d47339f9df0/canard_colvert_male.jpg?u=http://asset.keldelice.com/attachments/photos/583178/original/canard_colvert_male.jpg?1295444208&q=0&b=1&p=0&a=1)