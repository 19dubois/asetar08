-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 04 déc. 2020 à 21:30
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP :  7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `asetar2`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE `articles` (
  `idarticle` int(11) NOT NULL,
  `titrearticle` varchar(255) NOT NULL,
  `descriptionarticle` varchar(255) NOT NULL,
  `imagearticle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

CREATE TABLE `commandes` (
  `idcommande` int(11) NOT NULL,
  `idproduit` int(11) NOT NULL,
  `idcompte` int(11) NOT NULL,
  `datecommande` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

CREATE TABLE `commentaires` (
  `idcommentaire` int(11) NOT NULL,
  `idcompte` int(11) NOT NULL,
  `idpost` int(11) NOT NULL,
  `commentaire` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `comptes`
--

CREATE TABLE `comptes` (
  `idcompte` int(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `datenais` date NOT NULL,
  `etablissement` int(13) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tel` varchar(10) NOT NULL,
  `motdepasse` varchar(255) NOT NULL,
  `datedebutabonnement` date NOT NULL,
  `datefinabonnement` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `comptes`
--

INSERT INTO `comptes` (`idcompte`, `nom`, `prenom`, `pseudo`, `datenais`, `etablissement`, `email`, `tel`, `motdepasse`, `datedebutabonnement`, `datefinabonnement`) VALUES
(0, 'Jean', 'Michel', 'Bourrer', '1964-01-15', 0, 'Les@Alcolo.com', '0670809010', 'c88a65120330cfc69d5dbe1916fc8cd2', '0000-00-00', '0000-00-00'),
(1, 'Jean', 'Mil', 'Bourrer', '1964-01-15', 0, 'Les@Afecolo.com', '0670809010', 'c88a65120330cfc69d5dbe1916fc8cd2', '0000-00-00', '0000-00-00'),
(2, 'Mathis', 'Mantz', 'Teepride', '2001-04-22', 0, 'mantz.mathis22@gmail.com', '0662151675', '40883add63f1fbabc7fe6158f93c1246', '0000-00-00', '0000-00-00'),
(6, 'Marcellin', 'DUBOIS', 'marcod255', '2001-05-23', 1, 'marcellin.dubois@orange.fr', '0664169461', '0085b18f07f9a04ca548751067c6b44e', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Structure de la table `droits`
--

CREATE TABLE `droits` (
  `idcompte` int(11) NOT NULL,
  `idgrade` int(11) NOT NULL,
  `dategrade` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `droits`
--

INSERT INTO `droits` (`idcompte`, `idgrade`, `dategrade`) VALUES
(0, 3, '2020-12-02'),
(1, 3, '2020-12-03'),
(2, 3, '2020-12-03'),
(2, 3, '2020-12-03'),
(2, 3, '2020-12-03'),
(2, 3, '2020-12-03'),
(6, 3, '2020-12-03');

-- --------------------------------------------------------

--
-- Structure de la table `etablissements`
--

CREATE TABLE `etablissements` (
  `idetablissement` int(11) NOT NULL,
  `nometablissement` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `etablissements`
--

INSERT INTO `etablissements` (`idetablissement`, `nometablissement`) VALUES
(0, 'Lycée Gaspard Monge'),
(1, 'Lycée Bazin');

-- --------------------------------------------------------

--
-- Structure de la table `grades`
--

CREATE TABLE `grades` (
  `idgrade` int(12) NOT NULL,
  `nomgrade` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `grades`
--

INSERT INTO `grades` (`idgrade`, `nomgrade`) VALUES
(0, 'Administrateur'),
(1, 'Modérateur'),
(2, 'Membre'),
(3, 'Attente');

-- --------------------------------------------------------

--
-- Structure de la table `logs`
--

CREATE TABLE `logs` (
  `idlog` int(11) NOT NULL,
  `idcompte` int(11) NOT NULL,
  `activite` varchar(255) NOT NULL,
  `datelog` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

CREATE TABLE `panier` (
  `idpanier` int(11) NOT NULL,
  `idcompte` int(11) NOT NULL,
  `idproduit` int(11) NOT NULL,
  `quantitepanier` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `panier`
--

INSERT INTO `panier` (`idpanier`, `idcompte`, `idproduit`, `quantitepanier`) VALUES
(50, 0, 1, 1),
(51, 0, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `participants`
--

CREATE TABLE `participants` (
  `idparticipant` int(11) NOT NULL,
  `idcompte` int(11) NOT NULL,
  `idtournoi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

CREATE TABLE `post` (
  `idpost` int(11) NOT NULL,
  `titrepost` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `idcompte` int(11) NOT NULL,
  `idtag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

CREATE TABLE `produits` (
  `idproduit` int(11) NOT NULL,
  `nomproduit` varchar(255) NOT NULL,
  `ref` varchar(255) NOT NULL,
  `quantite` int(20) NOT NULL,
  `imageproduit` varchar(255) NOT NULL,
  `descriptif` varchar(255) NOT NULL,
  `prix` float NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`idproduit`, `nomproduit`, `ref`, `quantite`, `imageproduit`, `descriptif`, `prix`, `visible`) VALUES
(0, 'Canard', '345510055', 1, 'https://s2.qwant.com/thumbr/0x380/5/d/49208fe213b938d33b4bb07813d9ca097c1946eb18de73a85d9d47339f9df0/canard_colvert_male.jpg?u=http://asset.keldelice.com/attachments/photos/583178/original/canard_colvert_male.jpg?1295444208&q=0&b=1&p=0&a=1', 'Canard de bonne qualitée, pour faire un bon foie gras.', 200, 1),
(1, 'USB Canard', 'No16300050032', 5, 'https://images-na.ssl-images-amazon.com/images/I/61oxPE9F-EL._AC_SY355_.jpg', 'Capacité de stockage: 32 Go\r\nboîtier matériau: plastique\r\ntaille du produit: environ 30 mm x 36 mm x 40 mm\r\nSystèmes d\'exploitation supportés: Windows 98SE ou supérieur, Mac OS 9.0 ou supérieur\r\ncouleur: jaune', 10, 1);

-- --------------------------------------------------------

--
-- Structure de la table `rangs`
--

CREATE TABLE `rangs` (
  `idrang` int(11) NOT NULL,
  `idparticipant` int(11) NOT NULL,
  `rang` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE `tags` (
  `idtag` int(11) NOT NULL,
  `nomtag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tags`
--

INSERT INTO `tags` (`idtag`, `nomtag`) VALUES
(0, 'Tournoi');

-- --------------------------------------------------------

--
-- Structure de la table `tournois`
--

CREATE TABLE `tournois` (
  `idtournoi` int(11) NOT NULL,
  `nomtournoi` varchar(255) NOT NULL,
  `descriptiftournoi` varchar(255) NOT NULL,
  `datetournoi` date NOT NULL,
  `imagetournoi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`idarticle`);

--
-- Index pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD PRIMARY KEY (`idcommande`),
  ADD KEY `Foreigncomptecommande` (`idcompte`),
  ADD KEY `Foreignproduitcommande` (`idproduit`);

--
-- Index pour la table `commentaires`
--
ALTER TABLE `commentaires`
  ADD PRIMARY KEY (`idcommentaire`),
  ADD KEY `Fooreigncomptecommentaire` (`idcompte`),
  ADD KEY `Foreignpost` (`idpost`);

--
-- Index pour la table `comptes`
--
ALTER TABLE `comptes`
  ADD PRIMARY KEY (`idcompte`),
  ADD KEY `Foreign` (`etablissement`);

--
-- Index pour la table `droits`
--
ALTER TABLE `droits`
  ADD KEY `Foreigncompte` (`idcompte`),
  ADD KEY `Foreigngrade` (`idgrade`);

--
-- Index pour la table `etablissements`
--
ALTER TABLE `etablissements`
  ADD PRIMARY KEY (`idetablissement`);

--
-- Index pour la table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`idgrade`);

--
-- Index pour la table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`idlog`),
  ADD KEY `Foreigncomptelog` (`idcompte`);

--
-- Index pour la table `panier`
--
ALTER TABLE `panier`
  ADD PRIMARY KEY (`idpanier`),
  ADD KEY `Foreigncomptepanier` (`idcompte`),
  ADD KEY `Foreignproduitpanier` (`idproduit`);

--
-- Index pour la table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`idparticipant`),
  ADD KEY `Foreigncompteparticipant` (`idcompte`),
  ADD KEY `Foreigntournoi` (`idtournoi`);

--
-- Index pour la table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`idpost`),
  ADD KEY `Foreigncomptepost` (`idcompte`),
  ADD KEY `Foreigntag` (`idtag`);

--
-- Index pour la table `produits`
--
ALTER TABLE `produits`
  ADD PRIMARY KEY (`idproduit`);

--
-- Index pour la table `rangs`
--
ALTER TABLE `rangs`
  ADD PRIMARY KEY (`idrang`),
  ADD KEY `Foreignparticipant` (`idparticipant`);

--
-- Index pour la table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`idtag`);

--
-- Index pour la table `tournois`
--
ALTER TABLE `tournois`
  ADD PRIMARY KEY (`idtournoi`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `articles`
--
ALTER TABLE `articles`
  MODIFY `idarticle` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `commandes`
--
ALTER TABLE `commandes`
  MODIFY `idcommande` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `commentaires`
--
ALTER TABLE `commentaires`
  MODIFY `idcommentaire` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `comptes`
--
ALTER TABLE `comptes`
  MODIFY `idcompte` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `etablissements`
--
ALTER TABLE `etablissements`
  MODIFY `idetablissement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `grades`
--
ALTER TABLE `grades`
  MODIFY `idgrade` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `logs`
--
ALTER TABLE `logs`
  MODIFY `idlog` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `panier`
--
ALTER TABLE `panier`
  MODIFY `idpanier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT pour la table `participants`
--
ALTER TABLE `participants`
  MODIFY `idparticipant` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `post`
--
ALTER TABLE `post`
  MODIFY `idpost` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `produits`
--
ALTER TABLE `produits`
  MODIFY `idproduit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `rangs`
--
ALTER TABLE `rangs`
  MODIFY `idrang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tags`
--
ALTER TABLE `tags`
  MODIFY `idtag` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tournois`
--
ALTER TABLE `tournois`
  MODIFY `idtournoi` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `Foreigncomptecommande` FOREIGN KEY (`idcompte`) REFERENCES `comptes` (`idcompte`),
  ADD CONSTRAINT `Foreignproduitcommande` FOREIGN KEY (`idproduit`) REFERENCES `produits` (`idproduit`);

--
-- Contraintes pour la table `commentaires`
--
ALTER TABLE `commentaires`
  ADD CONSTRAINT `Fooreigncomptecommentaire` FOREIGN KEY (`idcompte`) REFERENCES `comptes` (`idcompte`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Foreignpost` FOREIGN KEY (`idpost`) REFERENCES `post` (`idpost`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `comptes`
--
ALTER TABLE `comptes`
  ADD CONSTRAINT `Foreign` FOREIGN KEY (`etablissement`) REFERENCES `etablissements` (`idetablissement`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `droits`
--
ALTER TABLE `droits`
  ADD CONSTRAINT `Foreigncompte` FOREIGN KEY (`idcompte`) REFERENCES `comptes` (`idcompte`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Foreigngrade` FOREIGN KEY (`idgrade`) REFERENCES `grades` (`idgrade`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `Foreigncomptelog` FOREIGN KEY (`idcompte`) REFERENCES `comptes` (`idcompte`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `panier`
--
ALTER TABLE `panier`
  ADD CONSTRAINT `Foreigncomptepanier` FOREIGN KEY (`idcompte`) REFERENCES `comptes` (`idcompte`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Foreignproduitpanier` FOREIGN KEY (`idproduit`) REFERENCES `produits` (`idproduit`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `participants`
--
ALTER TABLE `participants`
  ADD CONSTRAINT `Foreigncompteparticipant` FOREIGN KEY (`idcompte`) REFERENCES `comptes` (`idcompte`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Foreigntournoi` FOREIGN KEY (`idtournoi`) REFERENCES `tournois` (`idtournoi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `Foreigncomptepost` FOREIGN KEY (`idcompte`) REFERENCES `comptes` (`idcompte`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Foreigntag` FOREIGN KEY (`idtag`) REFERENCES `tags` (`idtag`);

--
-- Contraintes pour la table `rangs`
--
ALTER TABLE `rangs`
  ADD CONSTRAINT `Foreignparticipant` FOREIGN KEY (`idparticipant`) REFERENCES `participants` (`idparticipant`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
