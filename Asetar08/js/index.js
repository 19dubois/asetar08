/**
 * Initialisation ->
 * Lorsque que tout le contenu du DOM est chargé (DOMContentLoaded), on initialise la Sidenav de Materialize
 * https://materializecss.com/sidenav.html
 */

document.addEventListener("DOMContentLoaded", function () {
  var elems = document.querySelectorAll(".sidenav");
  var instances = M.Sidenav.init(elems);
});

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, options);
  });

document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.parallax');
  var instances = M.Parallax.init(elems, options);
});

$(document).ready(function(){
  $('.parallax').parallax();
});
