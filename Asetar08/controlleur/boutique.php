<?php
/**
 * boutique
 * 
 * S'occupe de l'affichage des données retournées de la boutique.
 * 
 */
class boutique
{
    private $titre = "Boutique";
    private $corps = "";
    private $redirection ="/asetar08-1/Asetar08/";
    private $idU = "";

    /**
     * Constructeur
     * 
     * Récupère les informations essencielles pour la boutique
     * Vérifie si la personne est connecté pour lancer la génération de la vue 
     * 
     */
    public function __construct()
    {
        //Ajout des pages à utiliser
        include_once "./modele/BoutiqueGestion.php";
        include_once "./modele/PanierGestion.php";
        //Si la session existe
        if(isset($_SESSION['email'])){
            $this->getIdUtilisateur();
        }
        if(isset($_POST["connexion"])){
            header("Location: ./?action=utilisateur");
        }
        // Affiche la vue boutique correspondant à l'URL
        if (isset($_GET["produit"]))
            {
                $this->corps = $this->getOneProduits($_GET["produit"]);
                $this->corps .= $this->getRandomProduits($this->nomproduit);
            }
        else
            {
                $this->corps = file_get_contents("./vue/menuBoutique.html");
                $this->corps .= $this->getProduits($this->getMenuProduit());
            }
        // Ajout au panier
        if (isset($_POST["panier"]))
            {
               $panier = new panierGestion();
               $panier->setPanier($this->idU,$_POST["panier"]);
               header('Location: ./?action=panier');
            }
    }

    /**
     * getIdUtilisateur
     * 
     * Récupération de l'id qui correspond au compte de l'utilisateur
     * 
     */
    public function getIdUtilisateur()
    {
        include_once './modele/UtilisateurGestion.php';
        $Utilisateur = new utilisateurGestion();
        $util = $Utilisateur->getUtilisateurByMailU($_SESSION['email']);
        if(!isset($util['idcompte'])){
            $this->idU="";
        }
        else{
            $this->idU=$util['idcompte'];
        }
    }

    /**
     * getMenuProduit
     * 
     * Permet d'effectuer la recherche grâce au nom du produit
     * 
     * @return String $value : correspond à la commande SQL effectuer pour la recherche
     * 
     */
    public function getMenuProduit():String
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['recherche']))
            {
                $value = " AND nomproduit LIKE '%";
                $value .= $_POST['recherche']."";
                $value .= "%'";
            }
        else
            {
                $value = " ORDER BY 1";
            }
        return $value;
    }
    /**
     * getProduits
     *
     *  Réalise et récupère la vue des produits
     * 
     * @param String $infosupp : Permet d'afficher d'ajouter des conditions SQL
     * @return String $Produitsresult : Renvoie la vue des étiquettes des produits
     * 
     */
    public function getProduits(String $infosupp):String
    {
        $boutique = new boutiqueGestion();
        $Produits = $boutique->listeProduit($infosupp);
        $Produitsresult = "<div class='row'><form action='.?action=boutique' method='POST'>";
        for ($i=0; $i < count($Produits) ; $i++)
            {
                $Produitsresult .= "<div class='col s12 m6 l3'><a href='".$this->redirection."?action=boutique&produit=".$Produits[$i]['idproduit']."'><div class='card'><div class='card-image'><img src='".$Produits[$i]['imageproduit']."' style='object-fit: cover;height:190px;'></div>";
                $Produitsresult .= "<div class='card-content'><span class='card-title'>".substr($Produits[$i]['nomproduit'], 0, 14)."</span><p>".$Produits[$i]['prix']."€</p></div>";
                if(isset($_SESSION['email'])){
                    $Produitsresult .= "<div class='card-action'><button class='btn waves-effect waves-light' name='panier' type='submit' value=".$Produits[$i]['idproduit'].">Acheter</button></div></a></div></div>";
                }else{
                    $Produitsresult .= "<div class='card-action'><button class='btn waves-effect waves-light' name='connexion' type='submit'>Acheter</button></div></a></div></div>";
                }
            }
        if(count($Produits) == 0){
            $Produitsresult .= "<h4>Aucun produit n'a été trouvé.</h4>" ;
        }
        $Produitsresult .= "</from></div>";
        return $Produitsresult;
    }

    /**
     * getRandomProduits
     * 
     * Prend 5 prduits au hasard et les proposes. 
     * 
     * @return String $Produitsresult : Renvoie la vue des étiquettes
     * 
     */
    public function getRandomProduits():String
    {
        $boutique = new boutiqueGestion();
        $Produits = $boutique->listeProduitRandom($_GET["produit"]);
        $Produitsresult = "<div class='row'>";
        for ($i=0; $i < 4 ; $i++)
            {
                if(isset($Produits[$i]['nomproduit'])){
                    $Produitsresult .= "<div class='col s12 m6 l3'><a href='".$this->redirection."?action=boutique&produit=".$Produits[$i]['idproduit']."'><div class='card'><div class='card-image'><img src='".$Produits[$i]['imageproduit']."' style='object-fit: cover;height:190px;'></div>";
                    $Produitsresult .= "<div class='card-content'><span class='card-title'>".substr($Produits[$i]['nomproduit'], 0, 15)."</span><p>".$Produits[$i]['prix']."€</p></div>";
                    $Produitsresult .= "</a></div></div>";
                }
            }
        $Produitsresult .= "</div>";
        return $Produitsresult;
    }

    /**
     * getOneProduits
     * 
     * Réalise la mise en page gâce aux informations récuperer sur un seul produit.
     * 
     * @param int $idproduit : Correspond à l'id du produit rechercher
     * @return String $Produitsresult : Renvoie les informations du produit
     * 
     */
    public function getOneProduits(int $idproduit):String
    {
        $boutique = new boutiqueGestion();
        $Produits = $boutique->OneProduit($idproduit);
        $Produitsresult = "<div class='row'><form action='.?action=boutique&produit=".$idproduit."' method='POST'>";
        $i=0;
        $Produitsresult .= "<div class='col s12 m4 l5'><img style='width:100%;' src='".$Produits[$i]['imageproduit']."'></div>";
        $Produitsresult .= "<div class='col s12 m4 l6'><h4>".$Produits[$i]['nomproduit']."</h4><p>".$Produits[$i]['descriptif']."</p></div>";
        $Produitsresult .= "<div class='col s12 m4 l1'><p>Quantité: ".$Produits[$i]['quantite']."</p></div>";
        $Produitsresult .= "</div><div class='row'><div class='col s12 m4 l4'><p style='margin-left:3%;'>Référence: ".$Produits[$i]['ref']."</p></div>";
        $Produitsresult .= "<div class='col s12 m4 l4'><p style='font-size:1.2em;'>Prix: ".$Produits[$i]['prix']."€</p></div>";
        if(isset($_SESSION['email'])){
            $Produitsresult .= "<div class='col s12 m4 l4' style='height:50px;'><button class='btn waves-effect waves-light' name='panier' type='submit' value=".$idproduit.">Acheter</button></div>";
        }else{
            $Produitsresult .= "<div class='col s12 m4 l4' style='height:50px;'><button class='btn waves-effect waves-light' name='connexion' type='submit'>Acheter</button></div>";
        }
        $Produitsresult .= "</form></div><br><br>";
        $Produitsresult .= "<h4 style='margin-left:2%;'>Autres produits:</h4><br>";
        $this->nomproduit = "".$Produits[$i]['nomproduit'];
        return $Produitsresult;
    }
    /**
     * corps
     *
     * Termine la génération du corps de la page
     * 
     */
    public function corps ()
    {
        $this->corps = $vuecorp;
    }

    /**
     * retour
     * 
     * Retourne la page terminée
     * 
     */
    public function retour():String
    {
        return($this->corps);
    }

    /**
     * retourTitre
     * 
     * Retourne  le titre de l'onglet
     * 
     */
    public function retourTitre():String
    {
        return($this->titre);
    }
}


?>
