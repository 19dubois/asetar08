<?php

class forum
{
    private $titre = "Forum";
    private $corps = "";
    private $redirection ="/asetar08-1/Asetar08/";
    
    
    public function __construct()
    {
        $this->AffichageForum();
    }
    
    public function AffichageForum()
    {
       $this->corps = file_get_contents("./vue/vueForum.html");
    }
   
    public function retour():String
    {
        return($this->corps);
    }
    public function retourTitre():String
    {
        return($this->titre);
    }
}
 
 
