<?php
/**
 * panier
 * 
 * S'occupe de l'affichage des données retournées par le panier.
 * 
 */
class panier
{
    private $titre = "Panier";
    private $corps = "";
    private $calcul = 0;
    private $nbarticle = 0;
    private $total = "";
    private $totalPrix = 0;
    private $panier=[];
    private $TVA = 20;
    private $Recaptitulatif = "";
    private $idU = "";
    private $Livraison = 5;

    /**
     * Constructeur
     * 
     * Récupère les informations essencielles pour le panier
     * Vérifie si la personne est connecté pour lancer la génération de la vue 
     * 
     */
    public function __construct()
    {
        include_once "./modele/PanierGestion.php";
        if(isset($_SESSION['email'])){
            $this->getIdUtilisateur();
            $Panier=$this->getPanier();
            $this->corps($Panier);
            //Selection de la méthode à utiliser pour effectuer la tâche.
            if($_SERVER["REQUEST_METHOD"] == "POST"){
                if(isset($_POST['delete'])){
                    $this->updatePanier($this->idU,$_POST['delete'],'delete');
                }
                if(isset($_POST['less'])){
                    if($_POST['less']!= ""){
                        $this->updatePanier($this->idU,$_POST['less'],'less');
                    }
                }
                if(isset($_POST['more'])){
                    if($_POST['more']!= ""){
                        $this->updatePanier($this->idU,$_POST['more'],'more');
                    }
                }
            }
        }else{
            $Panier=$this->getPanier();
            $this->corps($Panier);
        }
    }
    /**
     * updatePanier
     * 
     * @param int $idcompte : Correspond à l'id de compte de l'utilisateur
     * @param int $idproduit : Correspond à l'id du produit présent dans le panier
     * @param String $action : Correspond à l'action à utilit=ser
     */
    public function updatePanier(int $idcompte, int $idproduit,String $action)
    {
        if($action == "less" || $action == "more"){
            $update = new panierGestion();
            $update->updatePanier($idcompte,$idproduit,$action);
        }elseif($action == "delete"){
            $supprimer = new panierGestion();
            $supprimer->deletePanier($idcompte,$idproduit);
        }
        header("Refresh:0");
    }
    /**
     * getIdUtilisateur
     * 
     * Récupération de l'id qui correspond au compte de l'utilisateur
     * 
     */
    public function getIdUtilisateur()
    {
        include_once './modele/UtilisateurGestion.php';
        $Utilisateur = new utilisateurGestion();
        $util = $Utilisateur->getUtilisateurByMailU($_SESSION['email']);
        if(!isset($util['idcompte'])){
            $this->idU="";
        }
        else{
            $this->idU=$util['idcompte'];
        }
    }
    /**
     * getPanier
     * 
     * Récupère et renvoie l'affichage du panier
     * 
     * @return $Panierresult : Retourne l'affichage du panier
     */
    public function getPanier():String
    {
        if(isset($_SESSION['email'])){
        $votrePanier = new panierGestion();
        $Panier = $votrePanier->listePanier($this->idU);
        $this->panier = $Panier;
        $Panierresult = "";
        for ($i=0; $i < count($Panier) ; $i++)
            {
                $Panierresult .= "<div class='row'><div class='col s12 m4 l3'><img src='".$Panier[$i]['imageproduit']."' style='width:100%;'></div>";
                $Panierresult .= "<div class='col s12 m4 l3'><h5>".$Panier[$i]['nomproduit']."</h5></div>";
                $Panierresult .= "<div class='col s12 m4 l2'><h7 style='margin-left:auto;margin-right:auto;display:block;'><br><br>".$Panier[$i]['quantite']."</h7>";
                if($Panier[$i]['quantite'] != 1){
                    $Panierresult .= "<button class='btn waves-effect waves-light' style='width:1%;' name='less' type='submit' value=".$Panier[$i]['idproduit']."><i class='material-icons'>remove</i></button>";
                }else{
                    $Panierresult .= "<button class='btn waves-effect waves-light' style='width:1%;background-color:gray;' name='less' type='submit' value=''><i class='material-icons'>remove</i></button>";
                }
                if($Panier[$i]['quantitemax'] < $Panier[$i]['quantite']+1){
                    $Panierresult .= "<button class='btn waves-effect waves-light' style='width:1%;background-color:gray;' name='more' type='submit' value=''><i class='material-icons'>add</i></button></div>";
                }else{
                    $Panierresult .= "<button class='btn waves-effect waves-light' style='width:1%;' name='more' type='submit' value=".$Panier[$i]['idproduit']."><i class='material-icons'>add</i></button></div>";
                }
                $Panierresult .= "<div class='col s12 m4 l2'><h7><br><br>".$Panier[$i]['prix']."€</h7></div>";
                $Panierresult .= "<div class='col s12 m4 l2'><button class='btn waves-effect waves-light' style='width:100%;height:100%;margin-top: 35%;' name='delete' type='submit' value=".$Panier[$i]['idproduit']."><i class='material-icons' style='width:100%;height:100%;'>close</i></button></div></div>";
                $this->calcul = $this->calcul + $Panier[$i]['quantite'] * $Panier[$i]['prix'];
                $this->nbarticle = $this->nbarticle + $Panier[$i]['quantite'];
                $this->recapitulatif();
            }
        if(count($Panier)==0){
            $Panierresult .= "<div class='row'><div class='col s12 m4 l3'></div>";
                $Panierresult .= "<div class='col s12 m4 l5'><h5>Votre panier est vide.</h5></div>";
                $Panierresult .= "<div class='col s12 m4 l2'><h7><br><br></h7></div>";
                $Panierresult .= "<div class='col s12 m4 l2'><h7><br><br></h7></div></div>";
                $this->calcul = 0;
                $this->nbarticle = 0;
                $this->recapitulatif();
        }
    }else{
        $Panierresult = "";
        $Panierresult .= "<div class='row'><div class='col s12 m4 l3'></div>";
        $Panierresult .= "<div class='col s12 m4 l5'><h5>Votre panier est vide.</h5></div>";
        $Panierresult .= "<div class='col s12 m4 l2'><h7><br><br></h7></div>";
        $Panierresult .= "<div class='col s12 m4 l2'><h7><br><br></h7></div></div>";
        $this->calcul = 0;
        $this->nbarticle = 0;
        $this->recapitulatif(); 
    }
        $this->total = "(article ".$this->nbarticle."): ".$this->calcul."€"; 
        $Panierresult .= "";
        return $Panierresult;
    }
    /**
     * recapitulatif
     * 
     * Attribue à une variable, le récapitulatif de la commande
     * 
     */
    public function recapitulatif ()
    {
        $vueRecap = "<p> <b>Articles:</b><br>";
        for ($i=0; $i < count($this->panier) ; $i++)
            {
                $vueRecap .= "| ".$this->panier[$i]['nomproduit'].":   ";
                $vueRecap .= $this->panier[$i]['prix']."€ * ".$this->panier[$i]['quantite']."<br>";
            }
        if($this->calcul == 0)
        {
            $vueRecap .= "<b>Livraison:</b>    0€<br>";
        }else{
            $vueRecap .= "<b>Livraison:</b>    ".$this->Livraison."€<br>";  
        }
        $vueRecap .= "<b>Total HT:</b>    ".$this->calcul."€<br>";
        $calcul = $this->calcul * (20/100);
        $vueRecap .= "<b>TVA:</b>  ".$calcul."€<br>";
        $calcul = $this->calcul+$this->calcul * (20/100)+$this->Livraison;
        if($this->calcul == 0)
        {
            $vueRecap .= "<b>Montant total:    0€<br></b></p>";
        }else{
            $vueRecap .= "<b>Montant total:    ".$calcul."€<br></b></p>";
        }
        $this->Recaptitulatif = $vueRecap;
    }
    /**
     * corps
     *
     * Termine la génération du corps de la page
     * 
     */
    public function corps (String $Panier)
    {
        $vuecorp = file_get_contents("./vue/panier.html");
        $vuecorp = str_replace("%listecommande", $Panier,$vuecorp);
        $vuecorp = str_replace("%total", $this->total,$vuecorp);
        $vuecorp = str_replace("%recap", $this->Recaptitulatif,$vuecorp);
        $this->corps = $vuecorp;
    }
    /**
     * nombrepanier
     * 
     * Retourne le nombre d'article présent sur le panier.
     * 
     * @return array : renvoie un tableau avec le nombre d'article présent dans le panier de l'utilisateur
     * 
     */
    public function nombrePanier():array
    {
        include_once './modele/PanierGestion.php';
        if(isset($_SESSION['email'])){
        $panier = new panierGestion();
        $nombre=$panier->nombrePanier($this->idU);
        return $nombre;
        }else{
            return $nombre[0]['quantite'] = ['0'];
        }
    }
    /**
     * retour
     * 
     * Retourne la page terminée
     * 
     */
    public function retour():String
    {
        return($this->corps);
    }
    /**
     * retourTitre
     * 
     * Retourne  le titre de l'onglet
     * 
     */
    public function retourTitre():String
    {
        return($this->titre);
    }
}


?>