<?php
class utilisateur
{
    private $titre = "Authentification";
    private $corps = "";
    private $redirection ="/asetar08-1/Asetar08/";
    private $action = "connexion";
    private $LogPage = "";
    private $modification = "";
    private $Modif = "";
    private $Modif2 = "";
    public function __construct()
    {
        if (isset($_GET["status"]))
            {
                $this->action = $_GET["status"];
            }
            if (isset($_GET["modif"]))
            {
                $this->modification = $_GET["modif"];
            }
        include_once "./modele/UtilisateurGestion.php";
        $redirect = $this->FormUtilisateur($this->action);
        $this->corps($redirect);

        
    }
    public function FormUtilisateur(string $status)
    { 
        $Utilisateur = new utilisateurGestion();
        //$Cr = $Utilisateur->CreationUtilisateur("Jean","Mil","Bourrer","1964-01-15","0","Les@Afecolo.com","0670809010","zoe");
        if($status == "connexion")
        {
            if (!isset($_POST["email"]) || !isset($_POST["mdp"]))
                    {
                    // on affiche le formulaire de connexion
                    $redirection = "utilisateur";
                    }
                else
                    {
                    $email = $_POST["email"];
                    $mdp = $_POST["mdp"];
                
                    $Utilisateur->Connexion($email, $mdp);
                
                    if ($Utilisateur->isLoggedOn())
                        { // si l'utilisateur est connecté on redirige vers l'accueil
                            $redirection = "utilisateur";
                        }
                    else
                        {
                        // l'utilisateur n'est pas connecté, on affiche le formulaire de connexion
                        $redirection = "utilisateur";
                        }
                    
                    }
        }
        elseif($status == "inscription")
        {
            $redirection = "inscription";
        }
        elseif($status == "finalisation")
        {
            $redirection = "Final";
            $succes = $Utilisateur->CreationUtilisateur($_POST['nom'],$_POST['prenom'],$_POST['pseudo'],$_POST['date'],$_POST['etablissement'],$_POST['email'],$_POST['tel'],$_POST['mdp']);
            if($succes == TRUE)
            {
                $this->LogPage = "<div class='card-panel teal lighten-2'><h5>Compte crée avec succès</h5></div";
            }
            else
            {
                $this->LogPage = "<div class='card-panel teal lighten-2'><h5>Erreur dans la creation du compte veuillez reessayer (Si le probleme persiste Contacter l'administrateur du site)</h5></div";
            }
        }
        elseif($status == "deconnexion")
        {
            $Utilisateur->logout();
            $redirection = "utilisateur";
        }
        elseif($status == "Compte")
        {
            if($this->modification == "nom")
            {
                $Utilisateur->ModifUtilisateur($_POST["nomModif"],"nom");
                $Utilisateur->ModifUtilisateur($_POST["prenomModif"],"prenom");
            }
            elseif($this->modification == "pseudo")
            {
                $Utilisateur->ModifUtilisateur($_POST["pseudoModif"],"pseudo");
            }
            elseif($this->modification == "datenais")
            {
                $Utilisateur->ModifUtilisateur($_POST["dateModif"],"datenais");
            }
            elseif($this->modification == "etablissement")
            {
                $Utilisateur->ModifUtilisateur($_POST["etablissementModif"],"etablissement");
            }
            elseif($this->modification == "email")
            {
                $Utilisateur->ModifUtilisateur($_POST["emailModif"],"email");
            }
            elseif($this->modification == "tel")
            {
                $Utilisateur->ModifUtilisateur($_POST["telModif"],"tel");
            }
            $redirection = "compte";
        }
        elseif($status == "ModifCompte")
        {
            if($this->modification == "nom")
            {
                $this->Modif = "<!--%nom";
                $this->Modif2 = "%nom-->";
            }
            elseif($this->modification == "pseudo")
            {
                $this->Modif = "<!--%pseudo";
                $this->Modif2 = "%pseudo-->";
            }
            elseif($this->modification == "datenais")
            {
                $this->Modif = "<!--%datenais";
                $this->Modif2 = "%datenais-->";
            }
            elseif($this->modification == "etablissement")
            {
                $this->Modif = "<!--%etablissement";
                $this->Modif2 = "%etablissement-->";
            }
            elseif($this->modification == "email")
            {
                $this->Modif = "<!--%email";
                $this->Modif2 = "%email-->";
            }
            elseif($this->modification == "tel")
            {
                $this->Modif = "<!--%tel";
                $this->Modif2 = "%tel-->";
            }
            $redirection = "ModifCompte";
        }
    return $redirection;
    }   
    public function corps (String $redirection)
    {
        if($redirection == "inscription")
        {
            $LoginPage = file_get_contents("./vue/inscription.html");
            $this->titre = "Inscription";
        }
        elseif($redirection == "utilisateur")
        {   
            $LoginPage = file_get_contents("./vue/Connexion.html");
            $this->titre = "Authentification";
        }
        elseif($redirection == "Final")
        {
            $LoginPage = $this->LogPage;
            $this->titre = "Inscription";
        }
        elseif($redirection == "compte")
        {
            $Utilisateur = new utilisateurGestion();
            $tabcpt = $Utilisateur->getUtilisateurByMailU($_SESSION["email"]);
            $LoginPage = file_get_contents("./vue/Compte.html");
            $LoginPage = str_replace("%nom",$tabcpt['nom'],$LoginPage);
            $LoginPage = str_replace("%prenom",$tabcpt['prenom'],$LoginPage);
            $LoginPage = str_replace("%pseudo",$tabcpt['pseudo'],$LoginPage);
            $LoginPage = str_replace("%datenais",$tabcpt['datenais'],$LoginPage);
            $LoginPage = str_replace("%etablissement",$tabcpt['etablissement'],$LoginPage);
            $LoginPage = str_replace("%email",$tabcpt['email'],$LoginPage);
            $LoginPage = str_replace("%tel",$tabcpt['tel'],$LoginPage);
            $LoginPage = str_replace("%debutabo",$tabcpt['datedebutabonnement'],$LoginPage);
            $LoginPage = str_replace("%finabo",$tabcpt['datefinabonnement'],$LoginPage);

            $this->titre = "Compte";
        }
        elseif($redirection == "ModifCompte")
        {
            $LoginPage = file_get_contents("./vue/ModifCompte.html");
            $LoginPage = str_replace($this->Modif,"",$LoginPage);
            $LoginPage = str_replace($this->Modif2,"",$LoginPage);
        }
        $this->corps = $LoginPage;
    }
    public function retour():String
    {
        return($this->corps);
    }
    public function retourTitre():String
    {
        return($this->titre);
    }
}