<?php

class aide
{
    private $titre = "Aide";
    private $corps = "";
    private $redirection ="/asetar08-1/Asetar08/";
    
    
    public function __construct()
    {
        $this->AffichageCGU();
    }
    
    public function AffichageCGU()
    {
       $this->corps = file_get_contents("./vue/vueAide.html");
    }
   
    public function retour():String
    {
        return($this->corps);
    }
    public function retourTitre():String
    {
        return($this->titre);
    }
}
 
