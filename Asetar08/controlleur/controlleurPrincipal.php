<?php
class controleurPrincipal
{
    private $resultat;
    private $nomClass;
    private $corp;
    private $panierNumber;
    private $status = "<li><a href='./?action=utilisateur' title ='Connexion'>Connexion/Inscription</a></li>";
    private $status2 = "<a href ='./?action=utilisateur'>Connexion/Inscription</a>";
    /**
     * Construct
     */
    public function __construct()
    {
        include "accueil.php";
        include "boutique.php";
        include "Utilisateur.php";
        include "panier.php";
        include "cgu.php";
        include "forum.php";
        include "paypal.php";
        include "tournoi.php";
        include "aide.php";

        //Lancement de la session utilisateur
        session_start();
        if (isset($_GET["action"]))
            {
                $action = $_GET["action"];
            }
        else
            {
                $action = "defaut";
            }
        $this->gestion($action);
        $this->corp = new $this->nomClass();
        if ($this->corp->retour())
            {
                $titre = $this->corp->retourTitre();
                $this->resultat = $this->corp($titre, $this->corp->retour());
            }
        else
            {
                $titre = "404";
                $this->resultat = $this->corp("","<h1>ERREUR de chargement</h1>");
            }
    }

    /**
     * corp
     * 
     * @param string $titre title of tab
     * @param string $corp corresponding top and bottom html page (skeleton page)
     * @return string return full body of page
     */
    public function corp(string $titre, string $corp): string
    {
        include_once "./controlleur/panier.php";
        include_once "./modele/UtilisateurGestion.php";
        $this->StatusCo();
        $vuecorp = file_get_contents("./vue/pageModel.html");
        //<a href="" title ="Connexion/Inscription">Connexion/Inscription</a> -> %status
        $vuecorp = str_replace("%Titre", $titre,$vuecorp);
        $vuecorp = str_replace("%informationcentralepage", $corp,$vuecorp);
        $vuecorp = str_replace("%panierNumber", $this->panierNumber,$vuecorp);
        $vuecorp = str_replace("%status",$this->status,$vuecorp);
        $vuecorp = str_replace("%state2",$this->status2,$vuecorp);
        return $vuecorp;
    }
    /**
     * gestion
     * 
     * @param string $actions correspond action get in url
     */
    public function gestion(string $actions)
    {
        //Tableau des classes
        $lesActions = array();
        $lesActions["defaut"] = "accueil";
        $lesActions["accueil"] = "accueil";
        $lesActions["boutique"] = "boutique";
        $lesActions["utilisateur"] = "utilisateur";
        $lesActions["panier"] = "panier";
        $lesActions["cgu"] = "cgu";
        $lesActions["forum"] = "forum";
        $lesActions["aide"] = "aide";
        $lesActions["tournoi"] = "tournoi";
        $lesActions["paypal"] = "paypal";

        if (array_key_exists($actions, $lesActions))
            {
                $this->nomClass = $lesActions[$actions];
            }
        else
            {
                $this->nomClass = $lesActions["defaut"];
            }
    }
    /**
     * StatusCo
     * 
     * Verifie si la personne est connecté pour gérer la vue
     */
    public function StatusCo()
    {
        $utilisateur = new utilisateurGestion();
        $statusCo = $utilisateur->isLoggedOn();
        if($statusCo == true)
        {
            $this->status = "<li><a href='./?action=utilisateur&status=Compte' title ='Connexion'>Compte</a></li>";
            $this->status .= "<li><a href='./?action=utilisateur&status=deconnexion' title ='Connexion'>Deconnexion</a></li>";
            $this->status2 = "<a href='./?action=utilisateur&status=Compte'>Compte</a>";
            $this->nombreProduit();
        }
    }
    /**
     * nombreProduit
     * 
     * Si la personne est connecté, le nombre d'article présent dans le panier est afficher
     */
    public function nombreProduit()
    {
        $panier = new panier();
        $nombre=$panier->nombrePanier(0);
        if(isset($nombre[0]['quantite']))
        {
            if($nombre[0]['quantite'] != ['0'])
            {
                $this->panierNumber = "".$nombre[0]['quantite'];
            }else
            {
                $this->panierNumber = "";
            }
        }
    }
    
    /**
     *
     * @return string corresponding final client view
     */
    public function retour():string
    {
        return $this->resultat;
    }
}

?>
