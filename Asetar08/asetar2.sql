-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 05 mai 2021 à 09:25
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP :  7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `asetar2`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE `articles` (
  `idarticle` int(11) NOT NULL,
  `titrearticle` varchar(255) NOT NULL,
  `descriptionarticle` varchar(255) NOT NULL,
  `imagearticle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

CREATE TABLE `commandes` (
  `idcommande` int(11) NOT NULL,
  `idproduit` int(11) NOT NULL,
  `idcompte` int(11) NOT NULL,
  `datecommande` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

CREATE TABLE `commentaires` (
  `idcommentaire` int(11) NOT NULL,
  `idcompte` int(11) NOT NULL,
  `idpost` int(11) NOT NULL,
  `commentaire` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `comptes`
--

CREATE TABLE `comptes` (
  `idcompte` int(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `datenais` date NOT NULL,
  `etablissement` int(13) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tel` varchar(10) NOT NULL,
  `motdepasse` varchar(255) NOT NULL,
  `datedebutabonnement` date NOT NULL,
  `datefinabonnement` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `comptes`
--

INSERT INTO `comptes` (`idcompte`, `nom`, `prenom`, `pseudo`, `datenais`, `etablissement`, `email`, `tel`, `motdepasse`, `datedebutabonnement`, `datefinabonnement`) VALUES
(8, 'Mantz', 'Mathis', 'TeePride', '2001-04-22', 0, 'mantz.mathis22@gmail.com', '0612345789', '202cb962ac59075b964b07152d234b70', '0000-00-00', '0000-00-00'),
(9, 'DUBOIS', 'Marcellin', 'marcod255', '2001-05-23', 0, 'marcellin.dubois@orange.fr', '0664169461', '202cb962ac59075b964b07152d234b70', '0000-00-00', '0000-00-00'),
(10, 'Meunier', 'Jean', 'Villageois', '2001-07-17', 0, 'meunier.jean2001@gmail.com', '0000000000', 'f1b708bba17f1ce948dc979f4d7092bc', '0000-00-00', '0000-00-00'),
(11, 'Raphael', 'Antoine', 'james  james bond', '1999-09-04', 0, 'raphael.antoine99@gmail.com', '06 24 53 3', '75f3b7278f0ad6d75770d92fc6253e3b', '0000-00-00', '0000-00-00'),
(12, 'Marcellin', 'DUBOIS', 'marcod', '2001-05-23', 1, 'marcod.production@gmail.com', '0664169461', '202cb962ac59075b964b07152d234b70', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Structure de la table `droits`
--

CREATE TABLE `droits` (
  `idcompte` int(11) NOT NULL,
  `idgrade` int(11) NOT NULL,
  `dategrade` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `droits`
--

INSERT INTO `droits` (`idcompte`, `idgrade`, `dategrade`) VALUES
(8, 3, '2020-12-15'),
(9, 3, '2020-12-15'),
(10, 3, '2020-12-16'),
(11, 3, '2021-02-15'),
(12, 3, '2021-05-04');

-- --------------------------------------------------------

--
-- Structure de la table `etablissements`
--

CREATE TABLE `etablissements` (
  `idetablissement` int(11) NOT NULL,
  `nometablissement` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `etablissements`
--

INSERT INTO `etablissements` (`idetablissement`, `nometablissement`) VALUES
(0, 'Lycée Gaspard Monge'),
(1, 'Lycée Bazin');

-- --------------------------------------------------------

--
-- Structure de la table `grades`
--

CREATE TABLE `grades` (
  `idgrade` int(12) NOT NULL,
  `nomgrade` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `grades`
--

INSERT INTO `grades` (`idgrade`, `nomgrade`) VALUES
(0, 'Administrateur'),
(1, 'Modérateur'),
(2, 'Membre'),
(3, 'Attente');

-- --------------------------------------------------------

--
-- Structure de la table `logs`
--

CREATE TABLE `logs` (
  `idlog` int(11) NOT NULL,
  `idcompte` int(11) NOT NULL,
  `activite` varchar(255) NOT NULL,
  `datelog` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

CREATE TABLE `panier` (
  `idpanier` int(11) NOT NULL,
  `idcompte` int(11) NOT NULL,
  `idproduit` int(11) NOT NULL,
  `quantitepanier` float NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `panier`
--

INSERT INTO `panier` (`idpanier`, `idcompte`, `idproduit`, `quantitepanier`) VALUES
(91, 9, 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `participants`
--

CREATE TABLE `participants` (
  `idparticipant` int(11) NOT NULL,
  `idcompte` int(11) NOT NULL,
  `idtournoi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `payments`
--

CREATE TABLE `payments` (
  `id` int(6) NOT NULL,
  `txnid` varchar(20) NOT NULL,
  `payment_amount` decimal(7,2) NOT NULL,
  `payment_status` varchar(25) NOT NULL,
  `itemid` varchar(25) NOT NULL,
  `createdtime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

CREATE TABLE `post` (
  `idpost` int(11) NOT NULL,
  `titrepost` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `idcompte` int(11) NOT NULL,
  `idtag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` float(10,2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=Active | 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`id`, `name`, `image`, `price`, `status`) VALUES
(1, 'toi+moi', '...', 10.00, 1);

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

CREATE TABLE `produits` (
  `idproduit` int(11) NOT NULL,
  `nomproduit` varchar(255) NOT NULL,
  `ref` varchar(255) NOT NULL,
  `quantite` int(20) NOT NULL,
  `imageproduit` varchar(255) NOT NULL,
  `descriptif` varchar(255) NOT NULL,
  `prix` float NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`idproduit`, `nomproduit`, `ref`, `quantite`, `imageproduit`, `descriptif`, `prix`, `visible`) VALUES
(0, 'Canard', '345510055', 1, 'https://s2.qwant.com/thumbr/0x380/5/d/49208fe213b938d33b4bb07813d9ca097c1946eb18de73a85d9d47339f9df0/canard_colvert_male.jpg?u=http://asset.keldelice.com/attachments/photos/583178/original/canard_colvert_male.jpg?1295444208&q=0&b=1&p=0&a=1', 'Canard de bonne qualitée, pour faire un bon foie gras.', 200, 1),
(1, 'USB Canard', 'No16300050032', 5, 'https://images-na.ssl-images-amazon.com/images/I/61oxPE9F-EL._AC_SY355_.jpg', 'Capacité de stockage: 32 Go\r\nboîtier matériau: plastique\r\ntaille du produit: environ 30 mm x 36 mm x 40 mm\r\nSystèmes d\'exploitation supportés: Windows 98SE ou supérieur, Mac OS 9.0 ou supérieur\r\ncouleur: jaune', 10, 1),
(2, 'Porte-clés de canard au crochet', 'NO155641881', 10, 'https://static.wixstatic.com/media/a8e87a_d209cb7d343b462f93e567f7ccc14871~mv2.jpg/v1/fill/w_500,h_500,al_c,q_85,usm_0.66_1.00_0.01/a8e87a_d209cb7d343b462f93e567f7ccc14871~mv2.webp', 'J’aime faire des canards, et ces petits gars font des cadeaux idéaux, ou juste un petit régal pour vous-même! Ils sont tous faits de fil jaune, avec des yeux brodés noirs et des becs oranges. Ils sont tous à peu près de la même taille, 3cm de diamètre et ', 3.8, 1);

-- --------------------------------------------------------

--
-- Structure de la table `rangs`
--

CREATE TABLE `rangs` (
  `idrang` int(11) NOT NULL,
  `idparticipant` int(11) NOT NULL,
  `rang` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE `tags` (
  `idtag` int(11) NOT NULL,
  `nomtag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tags`
--

INSERT INTO `tags` (`idtag`, `nomtag`) VALUES
(0, 'Tournoi');

-- --------------------------------------------------------

--
-- Structure de la table `tournois`
--

CREATE TABLE `tournois` (
  `idtournoi` int(11) NOT NULL,
  `nomtournoi` varchar(255) NOT NULL,
  `descriptiftournoi` varchar(255) NOT NULL,
  `datetournoi` date NOT NULL,
  `imagetournoi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`idarticle`);

--
-- Index pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD PRIMARY KEY (`idcommande`),
  ADD KEY `Foreigncomptecommande` (`idcompte`),
  ADD KEY `Foreignproduitcommande` (`idproduit`);

--
-- Index pour la table `commentaires`
--
ALTER TABLE `commentaires`
  ADD PRIMARY KEY (`idcommentaire`),
  ADD KEY `Fooreigncomptecommentaire` (`idcompte`),
  ADD KEY `Foreignpost` (`idpost`);

--
-- Index pour la table `comptes`
--
ALTER TABLE `comptes`
  ADD PRIMARY KEY (`idcompte`),
  ADD KEY `Foreign` (`etablissement`);

--
-- Index pour la table `droits`
--
ALTER TABLE `droits`
  ADD KEY `Foreigncompte` (`idcompte`),
  ADD KEY `Foreigngrade` (`idgrade`);

--
-- Index pour la table `etablissements`
--
ALTER TABLE `etablissements`
  ADD PRIMARY KEY (`idetablissement`);

--
-- Index pour la table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`idgrade`);

--
-- Index pour la table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`idlog`),
  ADD KEY `Foreigncomptelog` (`idcompte`);

--
-- Index pour la table `panier`
--
ALTER TABLE `panier`
  ADD PRIMARY KEY (`idpanier`),
  ADD KEY `Foreigncomptepanier` (`idcompte`),
  ADD KEY `Foreignproduitpanier` (`idproduit`);

--
-- Index pour la table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`idparticipant`),
  ADD KEY `Foreigncompteparticipant` (`idcompte`),
  ADD KEY `Foreigntournoi` (`idtournoi`);

--
-- Index pour la table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`idpost`),
  ADD KEY `Foreigncomptepost` (`idcompte`),
  ADD KEY `Foreigntag` (`idtag`);

--
-- Index pour la table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `produits`
--
ALTER TABLE `produits`
  ADD PRIMARY KEY (`idproduit`);

--
-- Index pour la table `rangs`
--
ALTER TABLE `rangs`
  ADD PRIMARY KEY (`idrang`),
  ADD KEY `Foreignparticipant` (`idparticipant`);

--
-- Index pour la table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`idtag`);

--
-- Index pour la table `tournois`
--
ALTER TABLE `tournois`
  ADD PRIMARY KEY (`idtournoi`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `articles`
--
ALTER TABLE `articles`
  MODIFY `idarticle` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `commandes`
--
ALTER TABLE `commandes`
  MODIFY `idcommande` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `commentaires`
--
ALTER TABLE `commentaires`
  MODIFY `idcommentaire` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `comptes`
--
ALTER TABLE `comptes`
  MODIFY `idcompte` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `etablissements`
--
ALTER TABLE `etablissements`
  MODIFY `idetablissement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `grades`
--
ALTER TABLE `grades`
  MODIFY `idgrade` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `logs`
--
ALTER TABLE `logs`
  MODIFY `idlog` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `panier`
--
ALTER TABLE `panier`
  MODIFY `idpanier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT pour la table `participants`
--
ALTER TABLE `participants`
  MODIFY `idparticipant` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `post`
--
ALTER TABLE `post`
  MODIFY `idpost` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `produits`
--
ALTER TABLE `produits`
  MODIFY `idproduit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `rangs`
--
ALTER TABLE `rangs`
  MODIFY `idrang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tags`
--
ALTER TABLE `tags`
  MODIFY `idtag` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tournois`
--
ALTER TABLE `tournois`
  MODIFY `idtournoi` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `Foreigncomptecommande` FOREIGN KEY (`idcompte`) REFERENCES `comptes` (`idcompte`),
  ADD CONSTRAINT `Foreignproduitcommande` FOREIGN KEY (`idproduit`) REFERENCES `produits` (`idproduit`);

--
-- Contraintes pour la table `commentaires`
--
ALTER TABLE `commentaires`
  ADD CONSTRAINT `Fooreigncomptecommentaire` FOREIGN KEY (`idcompte`) REFERENCES `comptes` (`idcompte`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Foreignpost` FOREIGN KEY (`idpost`) REFERENCES `post` (`idpost`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `comptes`
--
ALTER TABLE `comptes`
  ADD CONSTRAINT `Foreign` FOREIGN KEY (`etablissement`) REFERENCES `etablissements` (`idetablissement`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `droits`
--
ALTER TABLE `droits`
  ADD CONSTRAINT `Foreigncompte` FOREIGN KEY (`idcompte`) REFERENCES `comptes` (`idcompte`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Foreigngrade` FOREIGN KEY (`idgrade`) REFERENCES `grades` (`idgrade`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `Foreigncomptelog` FOREIGN KEY (`idcompte`) REFERENCES `comptes` (`idcompte`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `panier`
--
ALTER TABLE `panier`
  ADD CONSTRAINT `Foreigncomptepanier` FOREIGN KEY (`idcompte`) REFERENCES `comptes` (`idcompte`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Foreignproduitpanier` FOREIGN KEY (`idproduit`) REFERENCES `produits` (`idproduit`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `participants`
--
ALTER TABLE `participants`
  ADD CONSTRAINT `Foreigncompteparticipant` FOREIGN KEY (`idcompte`) REFERENCES `comptes` (`idcompte`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Foreigntournoi` FOREIGN KEY (`idtournoi`) REFERENCES `tournois` (`idtournoi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `Foreigncomptepost` FOREIGN KEY (`idcompte`) REFERENCES `comptes` (`idcompte`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Foreigntag` FOREIGN KEY (`idtag`) REFERENCES `tags` (`idtag`);

--
-- Contraintes pour la table `rangs`
--
ALTER TABLE `rangs`
  ADD CONSTRAINT `Foreignparticipant` FOREIGN KEY (`idparticipant`) REFERENCES `participants` (`idparticipant`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
