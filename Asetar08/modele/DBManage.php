<?php
/**
 * ConnectPDOMySQL
 * 
 * Permet d'executer et de se connecter à la bdd
 * 
 */
class ConnectPDOMySQL
    {
    private $IP  = "localhost";
    private $BD  = "asetar2";
    private $LOG = "Asetar";
    private $MDP = "6FZFanjI6dpePI7A";
    private $ok  = false;
    private $mysql, $result;

/**
 * Constructeur de la classe 'ConnectPDOMySQL'
 */
    public function __construct ()
        {
        try
            {
            // Ouverture d'une connexion au SGBD MySQL ...
            $this->mysql = new PDO("mysql:host=" . $this->IP . ";dbname=" . $this->BD . ";charset=UTF8", $this->LOG, $this->MDP);
            if (!$this->mysql->errorCode()){
                $this->ok = true;
            }
            }
        catch (PDOException $e)
            {
            // Flag si erreur de connexion au SGBD ...
            $this->ok = false;
            }
        }
    /**
     * 
     * @param string $sql
     * @return string
     */
    public function SendSQL(string $sql)
        {
        if($this->ok == true)
            {
                $cnx = $this->mysql->prepare($sql);
                return $cnx;
            }
        }

/**
 * Status de la connexion MySQL
 * @return type statut de la connexion MySQL
 */
    public function getStatut() : bool
        {
        return $this->ok;
        }

    }
 
?>