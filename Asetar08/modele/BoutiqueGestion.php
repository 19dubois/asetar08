<?php
/**
 * boutiqueGestion
 * 
 * S'occupe de toute les demandes en rapport avec la boutique avec la base de donnée et renvoie les réponses
 */
class boutiqueGestion
{
  private $listSQLRequest;
  private $db;
  /**
   * Construction
   * 
   * Inclues les pages utiles pour les tâches à réaliser.
   */
  function __construct()
  {
    include_once 'DBManage.php';
    include 'ReqSQL.php';
    $this->db = new ConnectPDOMySQL();
    $this->listSQLRequest = $table_commande_sql;
  }

  /**
   * listeProduit
   * 
   * Réalise une requête SQL en rapport à la liste de produit
   * 
   * @param String $infosupp : Modifie la requête SQL par rapport à l'affichage demandée
   * @return array $resultat : Renvoie un tableau vide ou avec les informations demandée
   * 
   */
  public function listeProduit(String $infosupp):array
  {
    try
    {
      $command = $this->listSQLRequest[0][0].$infosupp;
      $value = $this->db->sendSQL($command);
      $value->execute();
      while ($ligne = $value->fetch(PDO::FETCH_ASSOC))
        {
        $resultat[] = $ligne;
        }
    } catch (PDOException $e){
      print "Erreur !: " . $e->getMessage();
      die();
    }
    if(!isset($resultat)){
      $resultat = [];
    }
    return $resultat;
  }

  /**
   * listeProduitRandom
   * 
   * Réalise une requête SQL en rapport à la liste de produit random, qui retourn des produits et leurs informations de manière aléatoire.
   * 
   * @param int $idproduit : Correspond au produit qui ne doit pas apparaître dans le tableau retourné.
   * @return array $resultat : Renvoie un tableau vide ou avec les informations demandée
   * 
   */
  public function listeProduitRandom(int $idproduit):array
  {
    try
    {
    $value = $this->db->sendSQL($this->listSQLRequest[0][2]);
    $value->bindValue(':idproduit', $idproduit, PDO::PARAM_INT);
    $value->execute();
    while ($ligne = $value->fetch(PDO::FETCH_ASSOC))
    {
      $resultat[] = $ligne;
    }

    } catch (PDOException $e){
      print "Erreur !: " . $e->getMessage();
      die();
    }
    if(!isset($resultat)){
      $resultat = [];
    }
    return $resultat;
  }
  /**
   * OneProduit
   * Réalise une requête SQL qui retourne les informations en rapport avec le produit.
   * 
   * @param int $idproduit : Correspond au produit qui doit apparaître dans le tableau retourné.
   * @return array $resultat : Renvoie un tableau vide ou avec les informations demandée
   * 
   */
  public function OneProduit(int $idproduit):array
  {
    try
    {
    $value = $this->db->sendSQL($this->listSQLRequest[0][1]);
    $value->bindValue(':idobject', $idproduit, PDO::PARAM_STR);
    $value->execute();
    while ($ligne = $value->fetch(PDO::FETCH_ASSOC))
    {
      $resultat[] = $ligne;
    }

    } catch (PDOException $e){
      print "Erreur !: " . $e->getMessage();
      die();
    }
    return $resultat;
  }

}
?>