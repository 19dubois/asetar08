<?php
/**
 * panierGestion
 * 
  * S'occupe de toute les demandes en rapport avec le panier avec la base de donnée et renvoie les réponses
 * 
 */
class panierGestion
{
  private $listSQLRequest;
  private $db;
  /**
   * Constructeur 
   * 
   * Inclues les pages utiles pour les tâches à réaliser.
   * 
   */
  function __construct()
  {
    include_once 'DBManage.php';
    include 'ReqSQL.php';
    $this->db = new ConnectPDOMySQL();
    $this->listSQLRequest = $table_commande_sql;
  }
  /**
   * listePanier
   * 
   * Renvoie la liste des produits qui se trouve dans le panier
   * 
   * @param int $idcompte : Correspond à l'id du compte utilisateur
   * @return array : Renvoie un tableau vide ou avec les informations demandées
   * 
   */
  public function listePanier(int $idcompte):array
  {
    try
    {
      $command = $this->listSQLRequest[2][0];
      $value = $this->db->sendSQL($command);
      $value->bindValue(':idcompte', $idcompte, PDO::PARAM_INT);
      $value->execute();
      while ($ligne = $value->fetch(PDO::FETCH_ASSOC))
        {
        $resultat[] = $ligne;
        }
    } catch (PDOException $e){
      print "Erreur !: " . $e->getMessage();
      die();
    }
    if(!isset($resultat)){
      $resultat = [];
    }

    return $resultat;
  }
  /**
   * nombrePanier
   * 
   * Renvoie le nombre de produit présent dans le panier
   * 
   * @param int $idcompte : Correspond à l'id du compte utilisateur
   * @return array : Renvoie un tableau vide ou avec les informations demandées
   * 
   */
  public function nombrePanier(int $idcompte):array
  {
    try
    {
      $command = $this->listSQLRequest[2][2];
      $value = $this->db->sendSQL($command);
      $value->bindValue(':idcompte', $idcompte, PDO::PARAM_INT);
      $value->execute();
      while ($ligne = $value->fetch(PDO::FETCH_ASSOC))
        {
        $resultat[] = $ligne;
        }
    } catch (PDOException $e){
      print "Erreur !: " . $e->getMessage();
      die();
    }
    if(!isset($resultat)){
      $resultat = [];
    }

    return $resultat;
  }

  /**
   * setPanier 
   * 
   * Ajoute un produit dans le panier.
   * 
   * @param int $idcompte : Correspond à l'id du compte utilisateur
   * @param int $idproduit : Correspond à l'id du produit qui doit être ajouter dans le panier
   * @return array : Renvoie un tableau vide ou avec les informations demandées
   * 
   */
  public function setPanier(int $idcompte, int $idproduit):array
  {
    try
    {
      $produitExistant = $this->verifierPanier($idcompte,$idproduit);
      if($produitExistant == [])
      {
        $command = $this->listSQLRequest[2][1];
        $value = $this->db->sendSQL($command);
      }
      else
      {
        $command = $this->listSQLRequest[2][4];
        $value = $this->db->sendSQL($command);
        $calcul = $produitExistant[0]['quantitepanier'] + 1;
        $value->bindValue(':quantitepanier', $calcul, PDO::PARAM_INT);
      }
      $value->bindValue(':idcompte', $idcompte, PDO::PARAM_INT);
      $value->bindValue(':idproduit', $idproduit, PDO::PARAM_INT);
      $value->execute();
      while ($ligne = $value->fetch(PDO::FETCH_ASSOC))
        {
        $resultat[] = $ligne;
        }
    } catch (PDOException $e){
      print "Erreur !: " . $e->getMessage();
      die();
    }
    if(!isset($resultat)){
      $resultat = [];
    }

    return $resultat;
  }

  /**
   * verifierPanier
   * 
   * Verifie si le produit n'est pas dans le panier.
   * 
   * @param int $idcompte : Correspond à l'id du compte utilisateur
   * @param int $idproduit : Correspond à l'id du produit qui doit être ajouter dans le panier
   * @return array : Renvoie un tableau vide ou avec les informations demandées
   */
  public function verifierPanier(int $idcompte, int $idproduit):array
  {
    try
    {
      $command = $this->listSQLRequest[2][3];
      $value = $this->db->sendSQL($command);
      $value->bindValue(':idcompte', $idcompte, PDO::PARAM_INT);
      $value->bindValue(':idproduit', $idproduit, PDO::PARAM_INT);
      $value->execute();
      while ($ligne = $value->fetch(PDO::FETCH_ASSOC))
        {
        $resultat[] = $ligne;
        }
    } catch (PDOException $e){
      die();
    }
    if(!isset($resultat)){
      $resultat = [];
    }

    return $resultat;
  }
  
  /**
   * deletePanier
   * 
   * Supprime le produit du panier
   * 
   * @param int $idcompte : Correspond à l'id du compte utilisateur
   * @param int $idproduit : Correspond à l'id du produit qui doit être ajouter dans le panier
   * @return array : Renvoie un tableau vide ou avec les informations demandées
   * 
   */
  public function deletePanier(int $idcompte, int $idproduit):array
  {
    try
    {
      $command = $this->listSQLRequest[2][5];
      $value = $this->db->sendSQL($command);
      $value->bindValue(':idcompte', $idcompte, PDO::PARAM_INT);
      $value->bindValue(':idproduit', $idproduit, PDO::PARAM_INT);
      $value->execute();
      while ($ligne = $value->fetch(PDO::FETCH_ASSOC))
        {
        $resultat[] = $ligne;
        }
    } catch (PDOException $e){
      print "Erreur !: " . $e->getMessage();
      die();
    }
    if(!isset($resultat)){
      $resultat = [];
    }
    return $resultat;
  }

  /**
   * updatePanier
   * 
   * Ajoute ou retire un article présent dans le panier
   * 
   * @param int $idcompte : Correspond à l'id du compte utilisateur
   * @param int $idproduit : Correspond à l'id du produit qui doit être ajouter dans le panier
   * @return array : Renvoie un tableau vide ou avec les informations demandées
   *  
   */
  public function updatePanier(int $idcompte, int $idproduit,String $action):array
  {
    try
    {
      if($action == "less"){
        $command = $this->listSQLRequest[2][6];
      }else{
        $command = $this->listSQLRequest[2][7];
      }
      $value = $this->db->sendSQL($command);
      $value->bindValue(':idcompte', $idcompte, PDO::PARAM_INT);
      $value->bindValue(':idproduit', $idproduit, PDO::PARAM_INT);
      $value->execute();
      while ($ligne = $value->fetch(PDO::FETCH_ASSOC))
        {
        $resultat[] = $ligne;
        }
    } catch (PDOException $e){
      print "Erreur !: " . $e->getMessage();
      die();
    }
    if(!isset($resultat)){
      $resultat = [];
    }
    return $resultat;
  }
}
?>