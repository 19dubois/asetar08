<?php


class utilisateurGestion
{
    private $listSQLRequest;
    private $db;
    function __construct()
    {
        include_once 'DBManage.php';
        include 'ReqSQL.php';
        $this->db = new ConnectPDOMySQL();
        $this->listSQLRequest = $table_commande_sql;
            
        
    }    
    /**
     * CréationUtilisateur : 
     *
     * @param  mixed $mailU
     * @param  mixed $mdpU
     * @param  mixed $pseudoU
     * @return void
     */
    public function CreationUtilisateur(string $nom,string $prenom,string $pseudoU,string $datenais,string $etablissement,string $email,string $tel,string $motdepasse)
    {
    try
        {
        $mdpCrypt = md5($motdepasse);
        $req = $this->db->SendSQL($this->listSQLRequest[1][0]);
            $req->bindValue(':nom', $nom, PDO::PARAM_STR);
            $req->bindValue(':prenom', $prenom, PDO::PARAM_STR);
            $req->bindValue(':pseudo', $pseudoU, PDO::PARAM_STR);
            $req->bindValue(':datenais', $datenais, PDO::PARAM_STR);
            $req->bindValue(':etablissement', $etablissement, PDO::PARAM_STR);
            $req->bindValue(':email', $email, PDO::PARAM_STR);
            $req->bindValue(':tel', $tel, PDO::PARAM_STR);
            $req->bindValue(':motdepasse', $mdpCrypt, PDO::PARAM_STR);
            $req->execute();
            $resultat = $req;
        $this->DroitsUtilisateur($nom,$email,"3");
        } catch (PDOException $e)
        {
        print "Erreur !: " . $e->getMessage();
        die();
        }
    return $resultat;
    }
    public function Connexion(string $email,string $mdp)
    {
        if (!isset($_SESSION))
        {
        session_start();
        }

    $util = $this->getUtilisateurByMailU($email);
    $mdpBD = $util["motdepasse"];
    if (trim($mdpBD) == trim(md5($mdp)))
        {
        // le mot de passe est celui de l'utilisateur dans la base de donnees
        $_SESSION["email"] = $email;
        $_SESSION["mdp"] = $mdpBD;
        }
    }
    /**
     * 
     * 
     * 
     */
    function isLoggedOn() : bool
    {
    if (!isset($_SESSION))
        {
        session_start();
        }
    $ret = false;

    if (isset($_SESSION["email"]))
        {
        $util = $this->getUtilisateurByMailU($_SESSION["email"]);
        if ($util["email"] == $_SESSION["email"] && $util["motdepasse"] == $_SESSION["mdp"]
        )
            {
            $ret = true;
            }
        }
    return $ret;
    }
    /**
     * 
     * 
     */
    function getUtilisateurByMailU(string $email):array
    {
    $resultat = array();

    try
        {
        $req = $this->db->SendSQL($this->listSQLRequest[1][3]);
        $req->bindValue(':email', $email, PDO::PARAM_STR);
        $req->execute();
        $resultat = $req->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e)
        {
        print "Erreur !: " . $e->getMessage();
        die();
        }
    return $resultat;
    }
    /**
     * 
     * 
     * 
     */
    function logout()
    {
    if (!isset($_SESSION))
        {
        session_start();
        }
    unset($_SESSION["email"]);
    unset($_SESSION["mdp"]);
    }
    public function ModifUtilisateur(string $modif,string $eqTable)
    {
        echo $eqTable;
        $Utilisateur = $this->getUtilisateurByMailU($_SESSION["email"]);
        try 
        {
            if($eqTable == "nom")
            {
                $req = $this->db->SendSQL($this->listSQLRequest[1][4]);
                $req->bindValue(':modif',$modif, PDO::PARAM_STR);
                $req->bindValue(':id',$Utilisateur['idcompte'], PDO::PARAM_STR);
                $req->execute();
                while ($ligne = $req->fetch(PDO::FETCH_ASSOC))
                {
                 $resultat[] = $ligne;
                } 
            }
            elseif($eqTable == "prenom")
            {
                $req = $this->db->SendSQL($this->listSQLRequest[1][5]);
                $req->bindValue(':modif',$modif, PDO::PARAM_STR);
                $req->bindValue(':id',$Utilisateur['idcompte'], PDO::PARAM_STR);
                $req->execute();
                while ($ligne = $req->fetch(PDO::FETCH_ASSOC))
                {
                    $resultat[] = $ligne;
                }
            }
            elseif($eqTable == "pseudo")
            {
                $req = $this->db->SendSQL($this->listSQLRequest[1][6]);
                $req->bindValue(':modif',$modif, PDO::PARAM_STR);
                $req->bindValue(':id',$Utilisateur['idcompte'], PDO::PARAM_STR);
                $req->execute();
                while ($ligne = $req->fetch(PDO::FETCH_ASSOC))
                {
                    $resultat[] = $ligne;
                }
            }
            elseif($eqTable == "datenais")
            {
                $req = $this->db->SendSQL($this->listSQLRequest[1][7]);
                $req->bindValue(':modif',$modif, PDO::PARAM_STR);
                $req->bindValue(':id',$Utilisateur['idcompte'], PDO::PARAM_STR);
                $req->execute();
                while ($ligne = $req->fetch(PDO::FETCH_ASSOC))
                {
                    $resultat[] = $ligne;
                }
            }
            elseif($eqTable == "etablissement")
            {
                $req = $this->db->SendSQL($this->listSQLRequest[1][8]);
                $req->bindValue(':modif',$modif, PDO::PARAM_STR);
                $req->bindValue(':id',$Utilisateur['idcompte'], PDO::PARAM_STR);
                $req->execute();
                while ($ligne = $req->fetch(PDO::FETCH_ASSOC))
                {
                    $resultat[] = $ligne;
                }
            }
            elseif($eqTable == "email")
            {
                $req = $this->db->SendSQL($this->listSQLRequest[1][9]);
                $req->bindValue(':modif',$modif, PDO::PARAM_STR);
                $req->bindValue(':id',$Utilisateur['idcompte'], PDO::PARAM_STR);
                $req->execute();
                while ($ligne = $req->fetch(PDO::FETCH_ASSOC))
                {
                    $resultat[] = $ligne;
                }
            }
            elseif($eqTable == "tel")
            {
                $req = $this->db->SendSQL($this->listSQLRequest[1][10]);
                $req->bindValue(':modif',$modif, PDO::PARAM_STR);
                $req->bindValue(':id',$Utilisateur['idcompte'], PDO::PARAM_STR);
                $req->execute();
                while ($ligne = $req->fetch(PDO::FETCH_ASSOC))
                {
                    $resultat[] = $ligne;
                }
            }
        }catch (PDOException $e)
        {
        print "Erreur !: " . $e->getMessage();
        die();
        } 
    }
    public function DroitsUtilisateur(string $nom,string $email,string $Droits)
    {
        try
        {
            $req = $this->db->SendSQL($this->listSQLRequest[1][1]);
            $req->bindValue(':nom', $nom, PDO::PARAM_STR);
            $req->bindValue(':email', $email, PDO::PARAM_STR);
            $req->execute();
            while ($ligne = $req->fetch(PDO::FETCH_ASSOC))
            {
            $resultat[] = $ligne;
            }
            $idcompte = $resultat[0];
            $insertion = $this->db->SendSQL($this->listSQLRequest[1][2]);
            $insertion->bindValue(':idcompte',$idcompte['idcompte'], PDO::PARAM_STR);
            $insertion->bindValue(':idgrade',$Droits, PDO::PARAM_STR);
            $insertion->bindValue(':dategrade',date("Y-m-d"), PDO::PARAM_STR);
            $insertion->execute();
        }catch (PDOException $e)
        {
        print "Erreur !: " . $e->getMessage();
        die();
        }   
        return $resultat;
    }
}